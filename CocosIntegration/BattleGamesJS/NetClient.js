import globals from "./globals";
import RuntimeCtrl from "./Runtime/RuntimeCtrl";

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        if (globals.NetClient) {
            console.error('There are more than 1 NetClient instance!!!');
        }
        globals.NetClient = this;

        this.webSocket = null;
        this.serverURL = '';
        this.roomID = '';
        this.playerID = '';
        this.userID = '';
        this.oppID = '';
    },

    start() {

        // this.tryToConnectToServer(`ws://94.237.79.169:8999`);
    },

    // update (dt) {},

    initServer(data) {


        this.serverURL = data.url;
        this.roomID = data.token.roomId;
        this.playerID = data.token.playerId;
        this.username = data.username;
        this.avatarURL = data.useravatar;


        // globals.Game.reset();
        globals.Game.setPlayerInfo(data);

        this.tryToConnectToServer(this.serverURL);
    },

    tryToConnectToServer: function (url) {

        this.webSocket = new WebSocket(url);
        console.log(`Trying to connect to server @ ${this.serverURL}`);

        const _this = this;

        this.webSocket.onopen = function (e) {

            console.log(`Connection established`);
            _this.onConnect();
        };

        this.webSocket.onerror = function (info) {

            console.log(`ConnectError: ${info}`);

            const data = {
                t: 'failed_to_connect_server',
                playerID: globals.NetClient.playerID
            };

            RuntimeCtrl.inst.sendCustomMsg({
                success() {
                    console.log("send connectionFailed msg: success");
                },
                fail() {
                    console.log("send connectionFailed msg: fail");
                }
            }, data);
        };
    },

    onConnect: function () {

        const _this = this;

        this.webSocket.onmessage = function (event) {

            _this.handleServerMessage(JSON.parse(event.data));
        }

        this.webSocket.onclose = function (event) {
            _this.onDisconnect(event);
        }

        const data = {
            t: 'ready',
            content: {
                username: this.username,
                userAvatarUrl: this.avatarURL,
                token: {
                    roomId: this.roomID,
                    playerId: this.playerID
                }
            }


        };
        this._sendWebsocketMessage(JSON.stringify(data));

    },

    onDisconnect: function (event) {

        console.log(`Disconnect from server!`);
        console.log(`Disconnect from server!`);
        const data = {
            t: 'on_disconnect',
            playerID: this.playerID
        };

        RuntimeCtrl.inst.sendCustomMsg({
            success() {
                console.log("send on_disconnect msg: success");
            },
            fail() {
                console.log("send on_disconnect msg: fail");
            }
        }, data);

    },

    handleServerMessage: function (data) {

        console.log(`Received msg:`);
        console.log(data);

        switch (data.t) {
            case 'start':
                this.userID = data.userID;
                this.oppID = data.oppID;
                globals.Game.setOpponentInfo(data);
                RuntimeCtrl.inst.sendCustomMsg({
                    success() {
                        console.log("send start msg: success");
                    },
                    fail() {
                        console.log("send start msg: fail");
                    }
                }, data);


                globals.Game.startCountdown();
                break;

            case 'state':

                globals.Game.setOppScore(data.score.score,data.score.data);
                break;

            case 'complete':
                globals.Game.gameComplete(data);
                RuntimeCtrl.inst.sendCustomMsg({
                    success() {
                        console.log("send GameComplete event: success");
                    },
                    fail() {
                        console.log("send GameComplete event: fail");
                    }
                }, data);

                break;
        }
    },

    sendPlayerScore: function (scData) {

        const msg = {
            t: 'state',
            score: scData
        };

        this._sendWebsocketMessage(JSON.stringify(msg));
    },
    sendGameOver: function (msg) {
        const data = {
            t: 'end',
            playerID: globals.NetClient.playerID,
            score: msg
        };
        try {
			
            this._sendWebsocketMessage(JSON.stringify(data));
        } catch (e) {
            console.log(e)
        }

        RuntimeCtrl.inst.sendCustomMsg({
            success() {
                console.log("send MyGameOver msg: success");
            },
            fail() {
                console.log("send MyGameOver msg: fail");
            }
        }, data);

    },
    sendPlayerAvatarClicked: function(){
        const data = {
                t: 'on_click_avatar',
                playerID: this.playerID
            };
    
            RuntimeCtrl.inst.sendCustomMsg({
                success() {
                    console.log("send GameComplete event: success");
                },
                fail() {
                    console.log("send GameComplete event: fail");
                }
            }, data);
        
        },
        
            sendOppponentAvatarClicked: function(){
        
         const data = {
                t: 'on_click_opp_avatar',
                playerID: this.playerID
            };
    
            RuntimeCtrl.inst.sendCustomMsg({
                success() {
                    console.log("send GameComplete event: success");
                },
                fail() {
                    console.log("send GameComplete event: fail");
                }
            }, data);
        },
    
    
    _sendWebsocketMessage: function (msg) {

        console.log('WebSocket send msg:');
        console.log(JSON.parse(msg));

        this.webSocket.send(msg);
    }
});
