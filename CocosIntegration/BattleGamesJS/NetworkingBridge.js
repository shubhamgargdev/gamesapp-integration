
import RuntimeCtrl from "./Runtime/RuntimeCtrl";
import globals from "./globals";

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad() {

        if (globals.Game) {
            console.error('There are more than 1 Game instance!!!');
        }
        globals.Game = this;
    },


    //#region Recieved callbacks

    setPlayerInfo(data) {
        // Call method to set player data here
     //   globals.GamePlay.setPlayerInfo(data);
    },

    setOpponentInfo(data) {
        // Call method to set opponent data here
     //   globals.GamePlay.setOpponentInfo(data);
    },

    startCountdown() {

        // Call method to start Game Here
      //  globals.GamePlay.startGame()
    },

    setOppScore(score, data) {
        console.log(score);
        console.log(data);
        // Call method to set opponent score here
      //  globals.GamePlay.setOppScore(score);
    },

    gameComplete(data) {

        console.log("rcvd game complete")
        let myScore, oppScore;
        for (let i = 0; i < 2; i++) {

            const info = data.room_users_score[i];
            if (info.user_id === globals.NetClient.userID) {
                myScore = info.user_score;
            } else {
                oppScore = info.user_score;
            }
        }
      //  globals.GamePlay.gameComplete(myScore, oppScore);


    },


    onDisconnect() {

        //should show a disconnected message here and stop game
    },

    //#endregion

    //#region Sending functions

    tellGameOver: function (score) {

        globals.NetClient.sendGameOver(score);
    },

    tellScore(playerScore, playerData = null) {
        let scData = {
            score: playerScore,
            data: playerData
        }
        globals.NetClient.sendPlayerScore(scData);
    },


    //Must call this method when player avatar is clicked
    onClickAvatar() {
        globals.NetClient.sendPlayerAvatarClicked();
    },

    //Must call this method when opponent avatar is clicked
    onClickOppAvatar() {

        globals.NetClient.sendOppponentAvatarClicked();
    },
});