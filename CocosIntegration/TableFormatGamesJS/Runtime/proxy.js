let config = {
	default: "1.0.0",
	getSystemInfoSync: "1.1.0",
	getBatteryInfo: {
		default: "1.0.0",
		isCharging: "1.1.0"
	},
	updateKeyboard: "1.4.0",
	getBatteryInfoSync: "1.1.0",
	getFileSystemManager: {
		default: "1.0.0",
		accessSync: "1.1.0",
		appendFile: "1.1.0",
		appendFileSync: "1.1.0",
		mkdirSync: "1.1.0",
		readFileSync: "1.1.0",
		renameSync: "1.1.0",
		rmdirSync: "1.1.0",
		readdirSync: "1.1.0",
		statSync: "1.1.0",
		unlinkSync: "1.1.0",
		writeFileSync: "1.1.0",
		saveFileSync: "1.1.0"
	},
	AudioEngine: {
		default: "1.0.0",
		setWaitingCallback: "1.1.0",
		setErrorCallback: "1.1.0",
	},
	uploadFile: "1.2.0",
	UploadTask: {
		default: "1.2.0",
		abort: "1.2.0",
		onProgressUpdate: "1.2.0",
	},
	getTextLineHeight: "1.0.2",
	loadFont: "1.2.0",
	setEnableDebug: "1.2.0",
	onWindowResize: "1.1.1",
	offWindowResize: "1.1.1",
	callCustomCommand: "1.1.1",
};
// let ui = require("ui");
let runtime = loadRuntime();
let coreVersion = "Less than 1.0.0";
if (runtime.getSystemInfo) {
	runtime.getSystemInfo({
		success(res) {
			coreVersion = res.coreVersion;
			if (!coreVersion) {
				coreVersion = res.COREVersion;
			}
		},
		fail() {
			coreVersion = "Less than 1.0.0";
		}
	});
}

/**
 * Do you need a proxy
 * @param obj   Inspection object
 * @returns {boolean}
 */
function isNeedProxy(obj) {
	let type = typeof obj;
	if (type === 'undefined' || obj === null || Array.isArray(obj) ||
		type === 'string' || obj instanceof String ||
		type === 'number' || obj instanceof Number ||
		type === 'boolean' || obj instanceof Boolean || obj instanceof ArrayBuffer) {
		return false;
	}
	return true;
}

/**
 * Attribute access interceptor
 * @param target Proxy object
 * @param key   Attribute name
 * @returns {*}
 */
function get(target, key) {
	let config = target.config;
	let version;
	/*
		Get the API configuration table of this interface
		1. default configuration-> existing configuration
		2. string configuration-> inherit configuration
		3. No configuration-> inherit parent configuration
	 */
	if (typeof config === "object") {
		if (typeof config[key] === "object") {
			version = config[key].default;
			config = config[key];
		} else {
			config = version = config[key] || config.default;
		}
	} else {
		version = config;
	}

	target = target.target;
	let value = target[key];
	let flag = true;

	/*
		Handling of existing interfaces
		For ios undefined = not implemented
		Whether agent
		Whether it is a function-> binding context

	 */
	if (key in target) {
		// For ios undefined
		if (value !== undefined) {
			if (!isNeedProxy(value)) {
				return value;
			} else if (typeof value === "function") {
				let fun = value.bind(target); // Binding context
				for (let key in value) {
					fun[key] = value[key];
				}
				value = fun;
			}
			flag = false;
		}
	}
	// ""+rt.aa -> get: key:symbol type
	if (typeof key === "symbol") {
		return function () { return "" };
	}

	// Unimplemented processing
	if (flag) {
		value = function () {
			return {};
        };
        // TODO: implement Alert popup
		// Prompt version
		// ui.open("ui_alert", {
		// 	title: "ui_alert",
		// 	content: "<color=#E7C5AD>Interface name：\n<color=#FFAB27>" + key + "</c>\nSupported version：<color=#FFAB27>" + version + "</c>\ncurrent version：<color=#FFAB27>" + coreVersion + "</c></c>"
		// });
	}

	// Iterative agent
	return buildProxy(value, config);
}

/**
 * Function call interceptor
 * @param target    Proxy object
 * @param context   Context (useless because it is proxied)
 * @param args      Call parameters
 * @returns {*}
 */
function apply(target, context, args) {
	let config = target.config;
	target = target.target;
	let value = target(...args);

	if (value === undefined) {
		value = function () { };
	} else {
		if (!isNeedProxy(value)) {
			return value;
		}
	}

	return buildProxy(value, config);
}

/**
 * Build agent
 * @param target Proxy object
 * @param config Object interface configuration table
 * @returns {proxyObj}
 */
function buildProxy(target, config) {
	let obj = {
		target: target,
		config: config
	};
	let proxyObj = function () { };
	proxyObj.prototype = obj;       // Take over function call，eg:rt.getFileSystemManager();
	Object.assign(proxyObj, obj);   // Take over property access，eg:rt.AudioEngine;

	return new Proxy(proxyObj, {
		get: get,
		apply: apply
	});
}

class RuntimeProxy {
    /**
     * Enable runtime proxy
     */
	static enabledProxy() {
		window.loadRuntime = function () {
			return buildProxy(runtime, config);
		}.bind(this);
	}

    /**
     * Close the runtime agent
     */
	static disabledProxy() {
		window.loadRuntime = function () {
			return runtime;
		}.bind(this);
	}
}

module.exports = RuntimeProxy;