import globals from "./globals";
import RuntimeCtrl from "./Runtime/RuntimeCtrl";

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:


    onLoad() {

        if (globals.NetClient) {
            console.error('There are more than 1 NetClient instance!!!');
        }
        globals.NetClient = this;
        this.serverURL = '';
        this.tableTypeID = '';
        this.playerID = '';
        this.avatarURL = '';
        this.playerName = '';
        this.token='';
    },



    initServer(data) {
        this.token = data.token;

        this.serverURL    = data.url;
        this.tableTypeID  = data.token.tableTypeID;
        this.playerID     = data.token.playerID;
        this.avatarURL    = data.useravatar;
        this.playerName   = data.username;

        let tablePlData = {
            serverURL: this.serverURL,
            playerID: this.playerID,
            avatarURL : this.avatarURL,
            playerName : this.playerName
        }

        globals.Game.setPlayerInfo(tablePlData);

    },


    sendExit: function (msg) {

        let exitData = {
            t: 'exit',
            reason: msg
        }

        RuntimeCtrl.inst.sendCustomMsg({
            success() {
                console.log("send exit msg: success");
            },
            fail() {
                console.log("send exit msg: fail");
            }
        }, exitData);

    },
    sendPlayerAvatarClicked: function () {
        const data = {
            t: 'on_click_avatar',
            playerID: this.playerID
        };

        RuntimeCtrl.inst.sendCustomMsg({
            success() {
                console.log("send GameComplete event: success");
            },
            fail() {
                console.log("send GameComplete event: fail");
            }
        }, data);

    },

    sendOppponentAvatarClicked: function (playerId) {

        const data = {
            t: 'on_click_opp_avatar',
            playerID: playerId
        };

        RuntimeCtrl.inst.sendCustomMsg({
            success() {
                console.log("send GameComplete event: success");
            },
            fail() {
                console.log("send GameComplete event: fail");
            }
        }, data);
    }
  

});
