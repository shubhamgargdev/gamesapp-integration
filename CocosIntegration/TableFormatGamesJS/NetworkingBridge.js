
import RuntimeCtrl from "./Runtime/RuntimeCtrl";
import globals from "./globals";

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad() {

        if (globals.Game) {
            console.error('There are more than 1 Game instance!!!');
        }
        globals.Game = this;
    },



    setPlayerInfo: function (data) {

    },

    sendExit: function (reasonLog) {

        globals.NetClient.sendExit();
    },

    //Must call this method when player avatar is clicked
    onClickAvatar() {
        globals.NetClient.sendPlayerAvatarClicked();
    },

    //Must call this method when opponent avatar is clicked
    onClickOppAvatar(playerId) {

        globals.NetClient.sendOppponentAvatarClicked(playerId);
    },

    onToggleAutoRefill(status) {

        globals.NetClient.sendAutoRefillStatus(status);
    },

});