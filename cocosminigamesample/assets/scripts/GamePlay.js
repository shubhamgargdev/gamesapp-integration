
import RuntimeCtrl from "./Runtime/RuntimeCtrl";
import globals from "./globals";

const { ccclass } = cc._decorator;

const State_WaitingServer = 'waiting';
const State_Counting = 'counting';
const State_Gameplay = 'gameplay';
const State_GameOver = 'gameover';

cc.Class({
    extends: cc.Component,

    properties: {
       
        countdown: {
            default: null,
            type: cc.Node
        },
        myScore: {
            default: null,
            type: cc.Label
        },
        playerName: {
            default: null,
            type: cc.Label
        },
        playerAvatar: {
            default: null,
            type: cc.Sprite
        },
        oppScore: {
            default: null,
            type: cc.Label
        },
        oppNameLabel: {
            default: null,
            type: cc.Label
        },
        oppAvatar: {
            default: null,
            type: cc.Sprite
        },
        cdLabel: {
            default: null,
            type: cc.Label
        },

        avatarBtn: {
            default: null,
            type: cc.Button
        },
        oppAvatarBtn: {
            default: null,
            type: cc.Button
        }
       
    },

    onLoad: function () {

        this.reset();

        this.avatarBtn.node.on(cc.Node.EventType.TOUCH_END, this.onClickAvatar, this);
        this.oppAvatarBtn.node.on(cc.Node.EventType.TOUCH_END, this.onClickOppAvatar, this);

        globals.GamePlay = this;
    },

    start: function () {

    },


    onClickAvatar: function () {
        globals.Game.onClickAvatar();
    },

    onClickOppAvatar: function () {
        globals.Game.onClickOppAvatar()

    },
    reset: function () {

        console.log(`Game reset()`);


        this.playerName.string = '...';
        this.myScore.string = 'You: 0';
        this.oppNameLabel.string = '...';
        this.oppScore.string = `Opp: 0`;

        this.gameOverMsg.getComponent(cc.Label).string = 'Game Over!!!';
        this.gameOverMsg.active = false;


    },


    setPlayerInfo: function (data) {

        this.username = data.username;
        this.avatarURL = data.useravatar;

        this.playerName.string = data.username;

        cc.loader.load(this.avatarURL, (err, tex) => {
            this.playerAvatar.spriteFrame = new cc.SpriteFrame(tex);
        });
    },

    setOpponentInfo: function (data) {

        this.oppName = data.oppName;
        this.oppAvatarURL = data.oppAvatarUrl;

        this.oppNameLabel.string = data.oppName;

        cc.loader.load(this.oppAvatarURL, (err, tex) => {
            this.oppAvatar.spriteFrame = new cc.SpriteFrame(tex);
        });
    },

    startGame: function () {
        this.score = 0
        this.countdown.active = true;
        this.cdLabel.string = "Game Started";
        console.log("Game has started");
        //this.setState( State_Counting);
    },


    setOppScore: function (score) {

        this.oppScore.string = `Opp: ${score}`;
    },


    gameComplete: function (myScore, oppScore) {

        this.myScore.string = 'Final: ' + myScore;
        this.oppScore.string = 'Final: ' + oppScore;


        this.cdLabel.string = 'Completed';

    },


    fakeConnectServer() {

        const data = {
            t: "connect_server",
            url: "ws://209.151.154.128:8999",
            username: "Shubham",
            useravatar: "https://i.imgur.com/3SGO7Vd.png",
            token: {
                roomId: '01a00d623aac128f751528eecabce2b8',
                playerId: '101e7ee9c0da21b5f5f7c9c356483ac51f67d466e98a0714479761594100367f1b8b5e63ced3c785047e86124add0bbe421a87d66590678d245b9fae897241caa4eaf2c1d214bd15c172191cf77c89d64b9d7c2a977094a87ba824af911328221cdeea22bc6fce5ee8a98259f1d52d74393079f4298ab11b6fbf75c52ee613afba139a9fcd9da0aee1594c85ddd1ab687b40ff4fcff36bf64c72e2e5aa69a670'
            }
        };

        globals.NetClient.initServer(data);
    },



    fakeConnectServer2() {

        const data = {
            t: "connect_server",
            url: "ws://209.151.154.128:8999",
            username: "Eric",
            useravatar: "https://i.imgur.com/0o8vwmK.png",
            token: {
                roomId: '1fe277a2453121d8fe42719b5af597f1',
                playerId: '1e0d3ea797291e9b185a68950150bfcb46c8c63f5625164d2da0c7d3b493b00d92a0b1e77ca788182325bf12ec315f0539901a9501cfb2e9109bc5734454b92df5d80ba08a80f280897ee2c31b920a676978f56d4ad8b16c2b9026b58744ac410029826385a5c611f87d6afed5bf229555cd55bba1861d6cd92aaef282d5fdf04d71cb2e8f7f3920956ab98712bcbbaa0072cd975445a365c7668c5b2f59be280ebf81197c341715d00ea86d7f2415fb'
            }
        };

        globals.NetClient.initServer(data);
    },


    fakeIncreaseScore: function () {
        this.score += 1;
        this.myScore.string = 'You: ' + this.score;

        let extraData = { lives: 15 }
        globals.Game.tellScore(this.score, extraData);
    },

    fakeGameOver: function () {

        globals.Game.tellGameOver(this.score);
    }

});