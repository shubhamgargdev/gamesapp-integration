import globals from "../globals";

class RuntimeCtrl {

    constructor() {

        window.runtimeController = this;
    }

    exitApplication ( callback) {

        loadRuntime().exitApplication( callback);
    }

    getServerDetailsFromJava ( json) {

        const data = JSON.parse( json);

        switch ( data.t) {
            case 'connect_server':
                globals.NetClient.initServer( data);
                break;
        }
    }

    sendCustomMsg ( callback, data) {

        loadRuntime().callCustomCommand( callback, JSON.stringify( data));
    }

    registerAudioInterruptionCallback ( cb) {

        loadRuntime().onAudioInterruptionBegin( cb);
    }

    registerOnHideCallback ( cb) {

        loadRuntime().onHide( cb);
    }
}


RuntimeCtrl.inst = new RuntimeCtrl();

export default RuntimeCtrl;